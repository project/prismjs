CKEditor5 Prism Js
=================

This module provides a way to allow editors to insert styled components
into CKEditor5 without having to grant the editors permission. Go to the
filter format you want to configure (must be using CKEditor5). Drag the
Prism Js icon into the active toolbar, and the config form will appear
below with a syntax highlighting style and supported languages option.
Configure the languages and theme from here /admin/config/content/prism-js.
By default, few  are checked for you.  Uncheck ones you won't need, it's
optional. This only controls the options in the dialog window of
CKEditor5 when inserting a source code.

Note that your filter format must support the use of pre and code tags
under allowed tags as well, if using anything other than Full HTML.
You also need to configure the HTML filter (if Limit Allowed Tags is
enabled) to allow the class attribute like so:

Usage
============

Enable the Prism Js text filter:

1. Create a new text filter or edit an existing one and enable CKEditor 5.
2. Drag the 'Prism Js' icon to the Ckeditor Toolbar
3. Enable the 'Prism Js' filter.


Prism Js Supported Languages
===============================

Prism Js supports 297 languages and 8 themes to check off, which will then
make them available in the dialog of CKEditor5 Prism Js.
