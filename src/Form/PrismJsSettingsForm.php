<?php

namespace Drupal\prismjs\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class PrismJsSettingsForm.
 *
 * This form allows site administrators to configure settings related
 * to the PrismJS integration.
 *
 * @package Drupal\prismjs\Form
 */
class PrismJsSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'prismjs_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getEditableConfigNames() {
    return ['prismjs.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('prismjs.settings');

    $form['language_config'] = [
      '#type' => 'details',
      '#title' => $this->t('Language Settings'),
      '#collapsible' => TRUE,
      '#open' => FALSE,
      '#description' => $this->t('Configure the available languages on the editor.'),
    ];

    $form['language_config']['languages'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Select available languages'),
      '#options' => prismjs_available_languages(),
      '#default_value' => $config->get('languages') ?: ['c', 'css', 'java', 'javascript', 'markup', 'php'],
      '#required' => TRUE,
    ];

    $form['theme'] = [
      '#type' => 'select',
      '#title' => $this->t('Default theme'),
      '#default_value' => $config->get('theme') ?: 'prism-tomorrow',
      '#options' => [
        "default" => $this->t("Default"),
        "dark" => $this->t("Dark"),
        "funky" => $this->t("Funky"),
        "okaidia" => $this->t("Okaidia"),
        "twilight" => $this->t("Twilight"),
        "coy" => $this->t("Coy"),
        "solarized-light" => $this->t("Solarized Light"),
        "tomorrow-night" => $this->t("Tomorrow Night"),
      ],
      '#description' => $this->t("Select the default theme"),
      '#required' => TRUE,
    ];

    $form['help'] = [
      '#type' => 'markup',
      '#markup' => '<small><em>Note: Flush all caches afer the configuration changes to take effect.</em></small>',
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('prismjs.settings');
    $values = $form_state->getValues();

    $config->set('languages', $values['languages']);
    $config->set('theme', $values['theme']);
    $config->save();

    parent::submitForm($form, $form_state);
  }

}
