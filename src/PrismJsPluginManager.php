<?php

namespace Drupal\prismjs;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Prism js plugin manager.
 */
class PrismJsPluginManager extends DefaultPluginManager {

  /**
   * Constructs Prism Js Plugin Manager.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/PrismJs',
      $namespaces,
      $module_handler,
      'Drupal\prismjs\PrismJsInterface',
      'Drupal\prismjs\Annotation\PrismJs'
    );
    $this->alterInfo('prismjs_info');
    $this->setCacheBackend($cache_backend, 'prismjs_plugins');
  }

}
