<?php

namespace Drupal\prismjs\Plugin\Filter;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\Renderer;
use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\prismjs\PrismJsPluginManager;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a text filter that turns < prism-js > tags into markup.
 *
 * @Filter(
 *   id = "prismjs",
 *   title = @Translation("Prism Js"),
 *   description = @Translation("Enable Prism Js source code syntax highlighter."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_REVERSIBLE,
 *   weight = 100,
 * )
 *
 * @internal
 */
class PrismJs extends FilterBase implements ContainerFactoryPluginInterface, TrustedCallbackInterface {

  /**
   * The plugin manager for prism js.
   *
   * @var \Drupal\prismjs\PrismJsPluginManager
   */
  protected $prismJsPluginManager;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, Renderer $renderer, PrismJsPluginManager $prismjs_plugin_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->prismJsPluginManager = $prismjs_plugin_manager;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('renderer'),
      $container->get('plugin.manager.ckedito5_prismjs')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {

    $matches = [];
    if (preg_match_all('/(?<code><prism-js.*?<\/prism-js>)/si', $text, $matches)) {

      $prismjs_config = \Drupal::config('prismjs.settings');
      $theme = $prismjs_config->get('theme') ?? 'tomorrow-night';

      $library = 'prismjs/prismjs.' . $theme;
      if (!empty($matches['code'])) {
        foreach ($matches['code'] as $code) {
          $source_code = $this->getStringBetween($code, 'data-plugin-config="', '"');
          if ($source_code) {
            $source_code = html_entity_decode($source_code);
            $source_code = json_decode($source_code);
            $text_content = $source_code->text ?? '';
            $language = $source_code->language ?? '';
            $text_content = htmlspecialchars($text_content);
            $replace = '<pre data-src="prism.js" class="language-' . $language . '"><code class="language-' . $language . '">' . $text_content . '</code></pre>';
            $text = str_replace($code, $replace, $text);
            $text = new FilterProcessResult($text);
            $text->addAttachments([
              'library' => [
                $library,
                'prismjs/prismjs.custom',
              ],
            ]);
          }
        }
      }
      return $text;
    }

    return new FilterProcessResult($text);
  }

  /**
   * Extracts a substring from a string.
   *
   * This function searches for the first occurrence of a start and
   * end string within the given input string, and returns the
   * substring found between them.
   *
   * @param string $string
   *   The input string to search within.
   * @param string $start
   *   The starting marker to begin the extraction.
   * @param string $end
   *   The ending marker to stop the extraction.
   *
   * @return string
   *   The substring found between the start and end markers. Returns
   *   an empty string if the markers are not found.
   */
  public function getStringBetween($string, $start, $end) {
    $string = ' ' . $string;
    $ini = strpos($string, $start);
    if ($ini == 0) {
      return '';
    }
    $ini += strlen($start);
    $len = strpos($string, $end, $ini) - $ini;
    return substr($string, $ini, $len);
  }

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks() {
    return [];
  }

}
