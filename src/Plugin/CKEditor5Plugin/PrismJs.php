<?php

declare(strict_types=1);

namespace Drupal\prismjs\Plugin\CKEditor5Plugin;

use Drupal\ckeditor5\Plugin\CKEditor5PluginDefault;
use Drupal\Core\Url;
use Drupal\editor\EditorInterface;

/**
 * Plugin class to add dialog url for prism js.
 */
class PrismJs extends CKEditor5PluginDefault {

  /**
   * {@inheritdoc}
   */
  public function getDynamicPluginConfig(array $static_plugin_config, EditorInterface $editor): array {
    $prismjs_dialog_url = Url::fromRoute('prismjs.dialog')
      ->toString(TRUE)
      ->getGeneratedUrl();
    $static_plugin_config['prismJs']['dialogURL'] = $prismjs_dialog_url;
    $prismjs_preview_url = Url::fromRoute('prismjs.preview', [
      'editor' => $editor->id(),
    ])
      ->toString(TRUE)
      ->getGeneratedUrl();
    $static_plugin_config['prismJs']['previewURL'] = $prismjs_preview_url;
    return $static_plugin_config;
  }

}
