<?php

namespace Drupal\prismjs\Plugin\PrismJs;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\prismjs\PrismJsInterface;
use Drupal\prismjs\PrismJsPluginBase;

/**
 * Plugin iframes.
 *
 * @PrismJs(
 *   id = "language_select",
 *   label = @Translation("Select Language"),
 *   description = @Translation("Renders a Language Selection."),
 * )
 */
class LanguageSelect extends PrismJsPluginBase implements PrismJsInterface {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'text' => NULL,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {

    return [
      '#theme' => 'prismjs_language_select',
      '#text' => $this->configuration['text'],
      '#attached' => [
        'library' => [
          'prismjs/prismjs.tomorrow-night',
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {

    $form['text'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Source Code'),
      '#default_value' => $this->configuration['text'],
      '#required' => TRUE,
    ];
    return $form;
  }

}
